<?php

declare(strict_types=1);

namespace App\Model;


/**
 * @property int $id
 * @property int $user_id
 * @property int $sku_id
 * @property int $quantity
 * @property int $status
 * @property \Carbon\Carbon $create_time
 * @property \Carbon\Carbon $update_time
 * @property int $delete_time
 */
class Cart extends BaseModel
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'cart';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'user_id' => 'integer', 'sku_id' => 'integer', 'quantity' => 'integer', 'status' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];

    /**
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function goods(): \Hyperf\Database\Model\Relations\HasOne
    {
        return $this->hasOne(Good::class, 'id', 'goods_id');
    }

    public function goodsSku()
    {
        return $this->hasOne(GoodsSku::class, 'id', 'sku_id');
    }
}
