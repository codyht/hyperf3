<?php

declare(strict_types=1);

namespace App\Model;



/**
 * @property int $id 
 * @property int $log_id 
 * @property int $log_add_id 
 * @property int $address_id 
 * @property string $city 
 * @property string $district 
 * @property string $county 
 * @property int $status 
 * @property \Carbon\Carbon $create_time 
 * @property \Carbon\Carbon $update_time 
 * @property int $delete_time 
 */
class LogisticsAddressId extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'logistics_address_ids';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'log_id' => 'integer', 'log_add_id' => 'integer', 'address_id' => 'integer', 'status' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];
}
