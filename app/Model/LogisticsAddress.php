<?php

declare(strict_types=1);

namespace App\Model;



/**
 * @property int $id 
 * @property int $log_id 
 * @property int $first 
 * @property string $price 
 * @property int $is_add 
 * @property string $add_price 
 * @property int $status 
 * @property \Carbon\Carbon $create_time 
 * @property \Carbon\Carbon $update_time 
 * @property int $delete_time 
 */
class LogisticsAddress extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'logistics_address';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'log_id' => 'integer', 'first' => 'integer', 'is_add' => 'integer', 'status' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];
}
