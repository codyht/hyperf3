<?php

declare(strict_types=1);

namespace App\Model;


/**
 * @property int $goods_id
 * @property int $user_id
 * @property \Carbon\Carbon $create_time
 */
class UserBrowsingHistory extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_browsing_history';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = ['number'];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['goods_id' => 'integer', 'user_id' => 'integer', 'create_time' => 'datetime'];
}
