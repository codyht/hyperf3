<?php

declare(strict_types=1);

namespace App\Model;


/**
 * @property int $id
 * @property int $user_id
 * @property int $parent_id
 * @property int $goods_id
 * @property string $comment
 * @property int $rating
 * @property int $status
 * @property \Carbon\Carbon $create_time
 * @property \Carbon\Carbon $update_time
 * @property int $delete_time
 */
class UserComment extends BaseModel
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_comments';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'user_id' => 'integer', 'parent_id' => 'integer', 'goods_id' => 'integer', 'rating' => 'integer', 'status' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];

    public function user()
    {
        return $this->hasMany(User::class, 'user_id', 'id');
    }

    public function goods()
    {
        return $this->hasMany(Good::class, 'goods_id', 'id');
    }
    
}
