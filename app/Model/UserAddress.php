<?php

declare(strict_types=1);

namespace App\Model;


/**
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $mobile
 * @property string $addressName
 * @property string $address
 * @property string $area
 * @property int $is_default
 * @property string $lat
 * @property string $lng
 * @property int $user_del
 * @property int $status
 * @property \Carbon\Carbon $create_time
 * @property \Carbon\Carbon $update_time
 * @property int $delete_time
 */
class UserAddress extends BaseModel
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_address';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'user_id' => 'integer', 'is_default' => 'integer', 'user_del' => 'integer', 'status' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];
}
