<?php

declare(strict_types=1);

namespace App\Model;



/**
 * @property int $id 
 * @property string $name 
 * @property string $value 
 * @property int $status 
 * @property int $creaet_time 
 * @property \Carbon\Carbon $update_time 
 * @property int $delete_time 
 */
class GoodsConfig extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'goods_config';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'status' => 'integer', 'creaet_time' => 'integer', 'update_time' => 'datetime', 'delete_time' => 'integer'];
}
