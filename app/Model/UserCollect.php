<?php

declare(strict_types=1);

namespace App\Model;


/**
 * @property int $user_id
 * @property int $goods_id
 */
class UserCollect extends Model
{
    protected bool $forceDeleting = false;
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'user_collect';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['user_id' => 'integer', 'goods_id' => 'integer'];

    /**
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function goods(): \Hyperf\Database\Model\Relations\HasOne
    {
        return $this->hasOne(Good::class, 'id', 'goods_id');
    }
}
