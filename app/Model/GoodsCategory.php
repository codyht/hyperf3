<?php

declare(strict_types=1);

namespace App\Model;


/**
 * @property int $id 
 * @property int $goods_id 
 * @property int $category_id 
 * @property int $status 
 * @property \Carbon\Carbon $create_time 
 * @property \Carbon\Carbon $update_time 
 * @property int $delete_time 
 * @property-read array $children 
 */
class GoodsCategory extends BaseModel
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'goods_category';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'goods_id' => 'integer', 'category_id' => 'integer', 'status' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];

    /**
     * @param $value
     * @return array
     */
    public function getChildrenAttribute($value): array
    {
        return $this->where('parent_id', $value)
            ->get(["id", "logo", "title", "parent_id"])->toArray();
    }
}
