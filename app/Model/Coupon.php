<?php

declare(strict_types=1);

namespace App\Model;



/**
 * @property int $id 
 * @property string $code 
 * @property string $price 
 * @property int $type 
 * @property int $coupon 
 * @property string $title 
 * @property string $discount 
 * @property int $start_time 
 * @property int $end_time 
 * @property \Carbon\Carbon $create_time 
 * @property \Carbon\Carbon $update_time 
 * @property int $delete_time 
 */
class Coupon extends Model
{
    /**
     * The table associated with the model.
     */
    protected ?string $table = 'coupon';

    /**
     * The attributes that are mass assignable.
     */
    protected array $fillable = [];

    /**
     * The attributes that should be cast to native types.
     */
    protected array $casts = ['id' => 'integer', 'type' => 'integer', 'coupon' => 'integer', 'start_time' => 'integer', 'end_time' => 'integer', 'create_time' => 'datetime', 'update_time' => 'datetime', 'delete_time' => 'integer'];
}
