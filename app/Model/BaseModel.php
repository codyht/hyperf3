<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/1
 * Time: 15:22
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Model;

use Hyperf\Database\Model\SoftDeletes;
use Hyperf\ModelCache\Cacheable;

class BaseModel extends Model
{
    /**
     * 软删除
     */
    use SoftDeletes;
}