<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/1
 * Time: 14:29
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Common\Business;

use App\Common\Lib\RequestUrl;
use App\Exception\FooException;
use App\Model\UserCollect;
use Hyperf\Context\ApplicationContext;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;

class UserCollectBusiness extends BusBase
{
    protected $obj_model;
    /**
     * @var AuthManager
     */
    #[Inject]
    protected AuthManager $auth;

    public function __construct()
    {
        $this->obj_model = new UserCollect();
    }

    /**
     * @param int $goods_id
     * @param int $user_id
     * @param int $sku_id
     * @param array $goods
     * @return true
     */
    public function setUserCollect(int $goods_id = 0, int $user_id = 0, int $sku_id = 0, array $goods = []): bool
    {
        try {
            $result = $this->getUserCollectByGoodsId($goods_id, $user_id);
            if (!empty($result)) {
                $res = $this->obj_model->where('goods_id', $goods_id)
                    ->where('user_id', $user_id)
                    ->delete();
            } else {
                $res = $this->obj_model->insert([
                    'goods_id' => $goods_id,
                    'user_id' => $user_id,
                    'sku_id' => $sku_id,
                    'title' => $goods['title'],
                    'url' => $goods['url'],
                    'price' => $goods['price']
                ]);
            }
        } catch (\Exception $e) {
            throw new FooException("查询失败");
        }
        if (!$res) {
            throw new FooException("收藏失败");
        }
        return true;
    }

    /**
     * @param int $goods_id
     * @param int $user_id
     * @return array
     */
    public function getUserCollectByGoodsId(int $goods_id = 0, int $user_id = 0)
    {
        try {
            $result = $this->obj_model->where('goods_id', $goods_id)
                ->where('user_id', $user_id)
                ->first();
        } catch (\Exception $e) {
            throw new FooException("查询失败" . $e->getMessage());
        }
        if (!$result) {
            return [];
        }
        return $result->toArray();
    }

    /**
     * @param int $limit
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getMemberIndex(int $limit = 10): array
    {
        $user_id = $this->auth->guard('jwt')->id();
        $url = ApplicationContext::getContainer()->get(RequestUrl::class)->getLocalUrlAndPort();
        try {
            $result = $this->obj_model
                ->where('user_id', $user_id)
                ->with('goods')
                ->paginate($limit)
                ->toArray();
        } catch (\Exception $e) {
            throw new FooException("查询错误");
        }
        $lists = [];
        if (!empty($result['data'])) {
            foreach ($result['data'] as $k => $v) {
                if (empty($v['goods'])) {
                    $lists[$k] = [
                        'title' => $v['title'],
                        'url' => $url . $v['url'],
                        'sku_id' => $v['sku_id'],
                        'price' => $v['price'],
                        'error_goods' => 1,
                        'id' => $v['id'],
                        'number' => 0
                    ];
                } else {
                    $lists[$k] = [
                        'id' => $v['goods']['id'],
                        'title' => $v['goods']['title'],
                        'url' => $url . $v['goods']['url'],
                        'sku_id' => $v['goods']['sku_id'],
                        'price' => $v['goods']['price'],
                        'error_goods' => 0,
                        'number' => $v['goods']['number']
                    ];
                }
            }
        }
        $result['data'] = $lists;
        return $result;
    }
}