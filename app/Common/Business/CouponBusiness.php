<?php
/**
 * Created by PhpStorm.
 * User: phptao
 * Email: 243194993@qq.com
 * Date: 2024/3/30
 * Time: 09:17
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Common\Business;

use App\Model\Coupon;

class CouponBusiness extends BusBase
{
    protected $obj_model;

    public function __construct()
    {
        $this->obj_model = new Coupon();
    }
}