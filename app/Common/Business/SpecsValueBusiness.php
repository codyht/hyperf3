<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/3/24
 * Time: 15:59
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Common\Business;

use App\Exception\FooException;
use App\Model\SpecsValue;

class SpecsValueBusiness extends BusBase
{
    protected $obj_model;

    public function __construct()
    {
        $this->obj_model = new SpecsValue();
    }

    /**
     * @param array $specs_id
     * @return array
     */
    public function getSpecsBySpecsName(array $specs_id = []): array
    {
        $arr = $this->getSpecsByInfo($specs_id);
        $result = [];
        if (!empty($arr)) {
            //var_dump(json_encode($arr, JSON_UNESCAPED_UNICODE));
            foreach ($arr as $v) {
                $result[$v['id']] = $v['specs']['title'];
            }
            //$result = array_column($specs, 'title', 'id');
        }
        //var_dump(json_encode($result));
        return $result;
    }

    /**
     * @param array $specs_id
     * @return array
     */
    public function getSpecsByInfo(array $specs_id = []): array
    {
        if (empty($specs_id)) {
            return [];
        }
        try {
            $result = $this->obj_model->whereIn('id', $specs_id)->with('specs')
                ->get()->toArray();
        } catch (\Exception $e) {
            throw new FooException("查询失败" . $e->getMessage());
        }
        return $result;
    }

    /**
     * @param array $specs_ids 所有specs_value ids
     * @param string $flagValue 选中的ids
     * @return array
     */
    public function getSpecsValueBySpecsName(array $specs_ids = [], string $flagValue = ''): array
    {
        if (empty($specs_ids)) {
            throw new FooException("内部异常");
        }
        $ids = [];
        $news = [];
        foreach ($specs_ids as $v) {
            $val = explode(',', (string)$v);
            foreach ($val as $k => $item) {
                $ids[$k][] = $item;
                $news[] = $item;
            }
        }
        $specsValueIds = array_unique($news);
        $specsValues = $this->getNormalInIds($specsValueIds);
        $flagValue = explode(',', $flagValue);
        $result = [];
        foreach ($ids as $key => $newValue) {
            $newsValue = array_unique($newValue);
            $lists = [];
            foreach ($newsValue as $vv) {
                $lists[] = [
                    'id' => $vv,
                    "name" => $specsValues[$vv]['name'],
                    'flag' => in_array($vv, $flagValue) ? 1 : 0
                ];
            }
            $result[$key] = [
                "name" => $specsValues[$newsValue[0]]['specs_name'],
                "list" => $lists
            ];
        }
        return $result;
    }

    /**
     * @param array $ids
     * @param array $where
     * @return array
     */
    public function getNormalInIds(array $ids = [], array $where = []): array
    {
        $result = parent::getNormalInIds($ids);
        $specs_ids = array_unique(array_column($result, 'specs_id'));
        try {
            $goodsSpecs = (new SpecsBusiness())->getNormalInIds($specs_ids);
        } catch (\Exception $e) {
            throw new FooException("查询错误");
        }
        $goodsSpecsValue = array_column($goodsSpecs, 'title', 'id');
        $res = [];
        foreach ($result as $v) {
            $res[$v['id']] = [
                'name' => $v['title'],
                'specs_name' => $goodsSpecsValue[$v['specs_id']] ?? ""
            ];
        }
        return $res;
    }
}