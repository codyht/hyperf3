<?php
/**
 * Created by PhpStorm.
 * User: phptao
 * Email: 243194993@qq.com
 * Date: 2024/3/29
 * Time: 18:46
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Common\Business;

use App\Exception\FooException;
use App\Model\GoodsConfig;
use App\Model\PayConfig;

class GoodsConfigBusiness extends BusBase
{
    protected $obj_model;

    public function __construct()
    {
        $this->obj_model = new GoodsConfig();
    }

    /**
     * @return array
     */
    public function getConfigPayTypeByList()
    {
        $type = $this->getConfigType('pay_type');
        if (!empty($type)) {
            $result = (new PayConfig())->query()
                ->whereIn('id', explode(',', $type['value']))
                ->get(['id as value', 'title as text', 'is_default']);
            if (!$result) {
                return [];
            } else {
                return $result->toArray();
            }
        } else {
            return [];
        }
    }

    /**
     * 获取配置
     * @param $type
     * @return array
     */
    public function getConfigType($type = '')
    {
        try {
            $result = $this->obj_model->query()
                ->where('name', $type)
                ->first();
        } catch (\Exception $e) {
            throw new FooException('配置错误');
        }
        if (!$result) {
            return [];
        } else {
            return $result->toArray();
        }
    }
}