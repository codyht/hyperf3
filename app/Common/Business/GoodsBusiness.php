<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/3/29
 * Time: 14:38
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Common\Business;


use App\Common\Lib\Log\Log;
use App\Common\Lib\RequestUrl;
use App\Common\Lib\Show;
use App\Constants\ErrorCode;
use App\Exception\FooException;
use App\Model\Good;
use Hyperf\Context\ApplicationContext;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Qbhy\HyperfAuth\AuthManager;

class GoodsBusiness extends BusBase
{
    protected $obj_model = "";
    /**
     * @var AuthManager
     */
    #[Inject]
    protected AuthManager $auth;

    public function __construct()
    {
        $this->obj_model = new Good();
    }

    /**
     * @param int $limit
     * @param string $tabType
     * @param array $field
     * @param string $orderKey
     * @param string $order_value
     * @return array
     */
    public function getGoodsIndex(int $limit = 10, string $tabType = 'normal', array $field = ['*'], string $orderKey = 'id', string $order_value = 'desc'): array
    {
        $where = null;
        $result = [];
        try {
            if ($tabType == 'all') {
                $result = $this->obj_model
                    ->whereIn('status', [0, 1])
                    ->orderBy($orderKey, $order_value)
                    ->paginate($limit, $field)->toArray();
            } else if ($tabType == 'second') {
                $result = $this->obj_model
                    ->where('status', '=', 2)
                    ->orderBy($orderKey, $order_value)
                    ->paginate($limit, $field)->toArray();
            } else if ($tabType == 'del') {
                $result = $this->obj_model
                    ->where('status', '=', 3)
                    ->orderBy($orderKey, $order_value)
                    ->paginate($limit, $field)->toArray();
            }
        } catch (\Exception $e) {
            throw new FooException("查询失败");
        }
        return $result;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getInfo(int $id = 0): array
    {
        if ($id == 0) {
            throw new FooException("内部异常");
        }
        $row = $this->getBaseById($id);

        $sku_data = [];
        if (!empty($row)) {
            if ($row['specification'] === 2) {
                $goods_sku = (new GoodsSkusBusiness())->getBaseByListInfo([
                    ['goods_id', '=', $row['id']]
                ]);
                $obj_specs_value = new SpecsValueBusiness();
                if (!empty($goods_sku)) {
                    //查询规格 todo 获取所有规格
                    $specs_ids = implode(',', array_column($goods_sku, 'specs_id'));
                    $arr_specs = array_unique(explode(',', $specs_ids));
                    $specs_value = $obj_specs_value->getSpecsByInfo($arr_specs);
                    if (!empty($specs_value)) {
                        foreach ($specs_value as $v) {
                            $v['checked'] = true;
                            if (isset($sku_data[$v['specs']['title']])) {
                                $sku_data[$v['specs']['title']]['child'][] = $v;
                            } else {
                                $sku_data[$v['specs']['title']] = [
                                    "title" => $v['specs']['title'],
                                    "child" => [$v]
                                ];
                            }
                        }
                    }
                    if (!empty($arr_specs)) {
                        $specs_name = $obj_specs_value->getSpecsBySpecsName($arr_specs);

                        foreach ($goods_sku as $k => $v) {
                            $specs_list = [];
                            if (!empty($v['specs_id'])) {
                                $specs_ids = explode(',', $v['specs_id']);
                                foreach ($specs_ids as $item) {
                                    $specs_list[] = [
                                        'id' => $item,
                                        'title' => $specs_name[$item]
                                    ];
                                }
                            }
                            $goods_sku[$k]['data_title'] = $specs_list;
                        }
                    }
                }
                $row['sku_data'] = array_values($sku_data);
                $row['sku'] = $goods_sku;

            }
            if (!empty($row['description'])) {
                $row['description'] = htmlspecialchars_decode($row['description']);
            }
            if (!empty($row['banner'])) {
                $row['banner'] = Show::json_decode($row['banner']);
                foreach ($row['banner'] as $k => $v) {
                    $row['banner'][$k] = [
                        "url" => $v
                    ];
                }
            }
            return $row;
        } else {
            throw new FooException("商品信息不存在");
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function add(array $data = []): bool
    {
        if (!empty($data['id']) && $data['id'] == 0) {
            unset($data['id']);
        }
        //判断是否选择了上传视频
        if ($data['is_video'] == 1) {
            if ($data['video_type'] == 1) {
                if (empty($data['video_url'])) {
                    throw new FooException("请上传视频");
                }
            } else {
                if (empty($data['video_url'])) {
                    throw new FooException("请输入视频地址");
                }
                if (!preg_match('/^https?:\/\/(.+\/)+.+(\.(swf|avi|flv|mpg|rm|mov|wav|asf|3gp|mkv|rmvb|mp4))$/i', $data['video_url'])) {
                    throw new FooException("请输入正确的地址");
                }
            }
        }
        //是否限购
        if ($data['is_purchase'] == 1) {
            if ($data['purchase_number'] == 0) {
                throw new FooException("请输入限购数量");
            }
        }
        //是否是预售
        if ($data['is_booking'] == 1) {
            if (empty($data['booking_time'])) {
                throw new FooException("请选择预售时间");
            }
        }
        //是否上传了banner
        if (empty($data['banner'])) {
            throw new FooException("请上传轮播图");
        }
        $banner_url = array_column($data['banner'], 'url');

        $data['banner'] = Show::json_encode($banner_url);
        if (!empty($data['recommend'])) {
            $data['recommend'] = Show::json_encode($data['recommend']);
        }
        if(empty($data['recommend'])){
            $data['recommend'] = Show::json_encode($data['recommend']);
        }
        if (!empty($data['logistics_type'])) {
            $data['logistics_type'] = Show::json_encode($data['logistics_type']);
        }
        if (!empty($data['description'])) {
            $data['description'] = htmlspecialchars($data['description']);
        }
        $sku_data = [];
        $cate_lists = [];
        $goods_img = "";
        if (!empty($data['category_id'])) {
            $cate = [];
            foreach ($data['category_id'] as $v) {
                if (count($v) >= 3) {
                    $cate[] = $v;
                    $cate_lists[] = [
                        "category_id" => end($v)
                    ];
                }
            }
            $data['category_list'] = Show::json_encode($cate);
            $data['category_id'] = "0";
        } else {
            throw new FooException("请选择商品分类");
        }
        if ($data['specification'] === 1) { //单规格
            if ($data['price'] === 0) {
                throw new FooException("请输入售价");
            }
            if ($data['market_price'] === 0) {
                throw new FooException("请输入成本价");
            }
            if ($data['cost_price'] === 0) {
                throw new FooException("请输入原价");
            }
            if ($data['stock'] === 0) {
                throw new FooException("请输入库存");
            }
            if (empty($data['goods_img'])) {
                throw new FooException("请上传商品图片");
            }
            $goods_img = $data['goods_img'];
        } else { //多规格
            if (empty($data['sku_data'])) {
                throw new FooException("请选择规格");
            }
            $is_false = false;
            $count_stock = 0;
            foreach ($data['sku_data'] as $v) {
                if (empty($v['specs_id'])) {
                    $is_false = true;
                    break;
                }
                if (empty($v['price'])) {
                    $is_false = true;
                    break;
                }
                if (empty($v['stock'])) {
                    $is_false = true;
                    break;
                }
                if (empty($v['cost_price'])) {
                    $is_false = true;
                    break;
                }
                if (empty($v['image'])) {
                    $is_false = true;
                    break;
                }
                if (empty($v['market_price'])) {
                    $is_false = true;
                    break;
                }
                $count_stock += $v['stock'];
                $data['price'] = $v['price'];
                $data['market_price'] = $v['market_price'];
                $data['cost_price'] = $v['cost_price'];
                $data['weight'] = $v['weight'];
                $data['volume'] = $v['volume'];
                $data['stock'] = $count_stock;
                $sku_data[] = [
                    'id' => $v['id'] ?? 0,
                    'price' => $v['price'],
                    'market_price' => $v['market_price'],
                    'cost_price' => $v['cost_price'],
                    'stock' => $v['stock'],
                    'image' => $v['image'],
                    'weight' => $v['weight'],
                    'volume' => $v['volume'],
                    'goods_id' => 0,
                    'specs_id' => $v['specs_id'],
                    'create_time' => time(),
                    'update_time' => time()
                ];
            }
            if ($is_false) {
                throw new FooException("请输入完规格在提交");
            }
        }
        if (!empty($data['booking_time'])) {
            $data['booking_time_start'] = strtotime($data['booking_time'][0]);
            $data['booking_time_end'] = strtotime($data['booking_time'][1]);
        }
        $casts = array_keys($this->obj_model->getCasts());
        foreach ($data as $k => $v) {
            if (!in_array($k, $casts)) {
                unset($data[$k]);
            }
        }
        $obj_goods_sku = new GoodsSkusBusiness();
        //添加入库
        Db::beginTransaction();
        try {
            //添加入库
            $data['create_time'] = time();
            $data['update_time'] = time();
            if (!empty($data['id'])) {
                $this->obj_model->where('id', $data['id'])->update($data);
                $goods_id = $data['id'];
            } else {
                $goods_id = $this->obj_model->insertGetId($data);
            }
            if (!$goods_id) {
                throw new FooException("添加失败");
            }
            //查询分类是否存在 存在分类
            (new GoodsCategoriesInfoBusiness())->setGoodsCategoriesList($cate_lists, $goods_id);
            $update_sku = [];
            if (!empty($data['id'])) {
                if ($data['specification'] == 1) {
                    $insert_goods_sku = [[
                        'price' => $data['price'],
                        'market_price' => $data['market_price'],
                        'cost_price' => $data['cost_price'],
                        'stock' => $data['stock'],
                        'image' => $goods_img,
                        'goods_id' => $goods_id,
                        'create_time' => time(),
                        'update_time' => time(),
                    ]];
                    $obj_goods_sku->updateData($insert_goods_sku[0], $goods_id);
                } else {
                    if ($sku_data[0]['id'] != 0) {
                        //更新sku
                        //更新多个sku
                        foreach ($sku_data as $k => $v) {
                            $v['goods_id'] = $goods_id;
                            $obj_goods_sku->updateDataBySkuData($v, $v['id']);
                        }
                    } else {
                        //直接删除 然后新增
                        $obj_goods_sku->delByData($goods_id);
                        $insert_goods_sku = [];
                        foreach ($sku_data as $k => $v) {
                            $insert_goods_sku[$k] = $v;
                            $insert_goods_sku[$k]['goods_id'] = $goods_id;
                        }
                    }
                }
            } else {
                if ($data['specification'] === 1) {
                    $insert_goods_sku = [[
                        'price' => $data['price'],
                        'market_price' => $data['market_price'],
                        'cost_price' => $data['cost_price'],
                        'stock' => $data['stock'],
                        'image' => $goods_img,
                        'goods_id' => $goods_id,
                        'create_time' => time(),
                        'update_time' => time()
                    ]];
                } else {
                    $insert_goods_sku = [];
                    foreach ($sku_data as $k => $v) {
                        if (!empty($v['id'])) {
                            unset($v['id']);
                        }
                        $insert_goods_sku[$k] = $v;
                        $insert_goods_sku[$k]['goods_id'] = $goods_id;
                    }
                }
//                $insert_sku_id = $obj_goods_sku->insertGetId($insert_goods_sku);
//                if (!$insert_sku_id) {
//                    throw new FooException("操作失败");
//                }
//                $insert_goods_sku_count = count($insert_goods_sku) - 1;
//                //更新主表
//                $this->obj_model->where('id', $goods_id)->update([
//                    'stock' => array_sum(array_column($insert_goods_sku, 'stock')),
//                    'sku_id' => $insert_sku_id,
//                    'url' => $insert_goods_sku[$insert_goods_sku_count]['image'],
//                    'market_price' => $insert_goods_sku[$insert_goods_sku_count]['market_price'],
//                    'cost_price' => $insert_goods_sku[$insert_goods_sku_count]['cost_price'],
//                ]);
            }
            if (!empty($insert_goods_sku)) {
                $insert_sku_id = $obj_goods_sku->insertGetId($insert_goods_sku);
                if (!$insert_sku_id) {
                    throw new FooException("操作失败");
                }
                $insert_goods_sku_count = count($insert_goods_sku) - 1;
                //更新主表
                $this->obj_model->where('id', $goods_id)->update([
                    'stock' => array_sum(array_column($insert_goods_sku, 'stock')),
                    'sku_id' => $insert_sku_id,
                    'url' => $insert_goods_sku[$insert_goods_sku_count]['image'],
                    'market_price' => $insert_goods_sku[$insert_goods_sku_count]['market_price'],
                    'cost_price' => $insert_goods_sku[$insert_goods_sku_count]['cost_price'],
                ]);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            Log::get('goods_add', 'error')->error($e->getMessage() . " " . $e->getFile() . " " . $e->getLine());
            throw new FooException("操作失败");
        }
        return true;
    }

    /**
     * 回收商品
     * @param int|string $id
     * @return bool
     */
    public function del(int|string $id = 0): bool
    {
        if ($id == 0) {
            throw new FooException("内部异常");
        }
        try {
            $res = $this->obj_model
                ->whereIn('id', explode(',', $id))->update([
                    'status' => 3
                ]);
        } catch (\Exception $e) {
            throw new FooException("删除异常");
        }
        if (!$res) {
            throw new FooException("删除失败");
        }
        return true;
    }

    /**
     * @param int $limit
     * @param int $cate_id
     * @param string $order_type
     * @param string $order_value
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getGoodsIndexListInfo(int $limit = 10, int $cate_id = 0, string $order_type = 'normal', string $order_value = ''): array
    {
        $field = "id,title,sku_id,type,status,specification,price,market_price,cost_price,stock,number,url";
        $url = ApplicationContext::getContainer()->get(RequestUrl::class)->getLocalUrlAndPort();
        $orderBy = "sort desc,id desc";
        if ($order_type == 'number') {
            $orderBy = "number desc,sort desc,id desc";
        } else if ($order_type == "price") {
            if ($order_value == 'asc') {
                $orderBy = "price asc,sort desc,id desc";
            } else if ($order_value == 'desc') {
                $orderBy = "goods.price desc,sort desc,id desc";
            }
        }
        if ($order_type == 'number') {
            $orderBy = "number desc,sort desc,id desc";
        } else if ($order_type == "price") {
            if ($order_value == 'asc') {
                $orderBy = "price asc,sort desc,id desc";
            } else if ($order_value == 'desc') {
                $orderBy = "price desc,sort desc,id desc";
            }
        }
        try {
            if (empty($cate_id) || $cate_id == 0) {

                $result = $this->obj_model->where('status', 1)
                    ->orderByRaw($orderBy)
                    ->paginate($limit, explode(',', $field))->toArray();
            } else {

                $result = $this->obj_model
                    ->join('goods_categories_info', 'goods.id', '=', 'goods_categories_info.goods_id')
                    ->where('goods_categories_info.category_id', $cate_id)
                    ->where('goods.status', ErrorCode::MYSQL_SUCCESS)
                    ->orderByRaw($orderBy)
                    ->paginate($limit, explode(',', $field))->toArray();
            }
        } catch (\Exception $e) {
            throw new FooException("查询失败" . $e->getMessage());
        }
        if (!empty($result['data'])) {
            foreach ($result['data'] as $k => $v) {
                $result['data'][$k]['url'] = $url . $v['url'];
            }
        }
        return $result;
    }

    /**
     * @param int $goods_id
     * @return true
     */
    public function setUserCollect(int $goods_id = 0): bool
    {
        $user_id = $this->auth->guard('jwt')->id();
        if ($goods_id == 0 || $user_id == 0) {
            throw new FooException("内部异常");
        }
        try {
            $goods = (new GoodsBusiness())->getBaseById($goods_id);
            $res = (new UserCollectBusiness())->setUserCollect($goods_id, $user_id, intval($goods['sku_id']), $goods);
        } catch (\Exception $e) {
            throw new FooException("操作失败");
        }
        return true;
    }

    /**
     * 商品详情
     * @param int $sku_id
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getGoodsDetailInfo(int $sku_id = 0): array
    {
        $user_id = $this->auth->guard('jwt')->id();
        $obj_goodsSku = new GoodsSkusBusiness();
        $url = ApplicationContext::getContainer()->get(RequestUrl::class)->getLocalUrlAndPort();
        //通过sku_id 查询所有商品id
        try {
            $goodsSku = $obj_goodsSku->getGoodsSkuByIDAttrGoods($sku_id);
        } catch (\Exception $e) {
            throw new FooException($e->getMessage());
        }
        $goods = $goodsSku['goods'];
        if (empty($goods)) {
            throw new FooException("商品信息不存在");
        }
        if (!empty($goods['url'])) {
            $goods['url'] = $url . $goods['url'];
        }
        if (!empty($goods['banner'])) {
            $goods['banner'] = Show::json_decode($goods['banner']);
            $goods['banner'] = array_map(function ($v) {
                $url = ApplicationContext::getContainer()->get(RequestUrl::class)->getLocalUrlAndPort();
                return $url . $v;
            }, $goods['banner']);
        }
        $goods['price'] = $goodsSku['price'];
        $goods['cost_price'] = $goodsSku['cost_price'];
        $goods['market_price'] = $goodsSku['market_price'];
        $goods['stock'] = $goodsSku['stock'];
        //获取所有goodsSku信息
        try {
            $skuGoods = $obj_goodsSku->getSkuByGoodsId($goods['id']);
        } catch (\Exception $e) {
            throw new FooException($e->getMessage());
        }
        //循环默认选中
        $flagValue = '';
        foreach ($skuGoods as $v) {
            if ($v['id'] == $sku_id) {
                $flagValue = $v['specs_id'];
            }
        }
        $goods_ids = array_column($skuGoods, 'id', 'specs_id');
        $goods['goods_ids'] = $goods_ids;
        $goods['description'] = preg_replace('/(<img.+?src=")(.*?)/', '$1' . $url . "$2", htmlspecialchars_decode($goods['description']));
        //获取商品sku
        if ($goods['specification'] === 2) {
            $specs_keys = array_keys($goods_ids);
            $goods['sku'] = (new SpecsValueBusiness())->getSpecsValueBySpecsName($specs_keys, $flagValue);
        } else {
            $goods['sku'] = [];
        }
        $goods['discount'] = intval((1 - ($goods['price'] / $goods['cost_price'])) * 10);

        //查询评论
        try {
            $comments = (new UserCommentsBusiness())->getUserCommentsByGoodsId($goods['id']);
        } catch (\Exception $e) {
            throw new FooException($e->getMessage());
        }
        $goods['comments'] = [
            "list" => $comments['data'],
            "count" => $comments['total']
        ];
        //增加浏览记录 返回总的浏览记录
        try {
            $count = (new UserBrowsingHistoryBusiness())->getUserHistoryInfo($goods['id'], $user_id);
        } catch (\Exception $e) {
            throw new FooException($e->getMessage());
        }
        $goods['browsing_number'] = $count;
        //查询收藏
        try {
            $collect = (new UserCollectBusiness())->getUserCollectByGoodsId($goods['id'], $user_id);
        } catch (\Exception $e) {
            throw new FooException($e->getMessage());
        }
        if (empty($collect)) {
            $goods['collect'] = 0;
        } else {
            $goods['collect'] = 1;
        }
        return $goods;
    }
}