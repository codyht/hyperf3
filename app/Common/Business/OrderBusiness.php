<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/2
 * Time: 10:08
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Common\Business;

use App\Common\Lib\RequestUrl;
use App\Exception\FooException;
use App\Model\GoodsSku;
use App\Model\Order;
use Hyperf\Context\ApplicationContext;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;

class OrderBusiness extends BusBase
{
    protected $obj_model = "";
    /**
     * @var AuthManager
     */
    #[Inject]
    protected AuthManager $auth;

    public function __construct()
    {
        $this->obj_model = new Order();
    }

    public function confirm(string $id = '', string $type = 'order')
    {
        if (empty($id)) {
            throw new FooException("内部异常");
        }
        $user_id = $this->auth->guard('jwt')->id();
        $url = ApplicationContext::getContainer()->get(RequestUrl::class)->getLocalUrlAndPort();

        try {
            if ($type == 'order') {
                $id = [intval($id)];
                $sku_data = (new GoodsSku())
                    ->whereIn('id', $id)
                    ->with(['goods'])
                    ->get()
                    ->toArray();
            } else {
                $id = explode(',', $id);
                $sku_data = (new CartBusiness())->getCartGoodsId($id, $user_id);
            }
        } catch (\Exception $e) {
            throw new FooException("查询失败");
        }
        if (empty($sku_data)) {
            throw new FooException("商品信息不存在");
        }
        $result = [];
        $goods = [];
        foreach ($sku_data as $k => $v) {
            if (!empty($v['goods'])) {
                if ($type == 'cart') {
                    $v['id'] = $v['sku_id'];
                    $v['specs_id'] = $v['goods_sku']['specs_id'];
                    $v['price'] = $v['goods_sku']['price'];
                    $v['image'] = $v['goods_sku']['image'];
                    $v['cost_price'] = $v['goods_sku']['cost_price'];
                    $v['stock'] = $v['goods_sku']['stock'];
                }
                if ($v['goods']['specification'] == 2) {
                    $specsName = (new SpecsValueBusiness())->getNormalInIds(explode(',', $v['specs_id']));
                    $sku_name = implode(' ', array_column($specsName, 'name'));
                } else {
                    $sku_name = '普通规格';
                }
                $goods_data = [
                    'id' => $v['goods']['id'],
                    'sku_id' => $v['id'],
                    'specs_id' => $v['specs_id'],
                    'specs_name' => $sku_name,
                    'price' => $v['price'],
                    'image' => $url . $v['image'],
                    'cost_price' => $v['cost_price'],
                    'stock' => $v['stock'],
                    'number' => $v['quantity'] ?? 1,
                    'title' => $v['goods']['title'],
                    'logistics_type' => $v['goods']['logistics_type'],   //配送方式
                ];
                if (isset($goods[$v['goods']['store_id']])) {
                    $goods[$v['goods']['store_id']]['list'][] = $goods_data;
                } else {
                    //获取门店信息
                    $store = (new StoreBusiness())->getBaseById($v['goods']['store_id']);
                    $goods[$v['goods']['store_id']] = [
                        'shop' => $store['title'] ?? '本店',
                        'logo' => $store['logo'] ?? '',
                        'list' => [$goods_data]
                    ];
                }

            }
        }
        $result['goods'] = array_values($goods);
        //查询用户默认地址
        try {
            $address = (new UserAddressBusiness())->getUserAddressInfo($user_id);
        } catch (\Exception $e) {
            throw new FooException($e->getMessage());
        }
        $result['address'] = $address;
        //查询是否有优惠券
        $result['coupon'] = [1];
        //支付方式
        $result['payType'] = (new GoodsConfigBusiness())->getConfigPayTypeByList();
        return $result;
    }
}