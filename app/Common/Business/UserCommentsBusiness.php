<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/1
 * Time: 14:12
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Common\Business;

use App\Common\Lib\Log\Log;
use App\Exception\FooException;
use App\Model\UserComment;

class UserCommentsBusiness extends BusBase
{
    /**
     * @var UserComment
     */
    protected $obj_model;

    /**
     *
     */
    public function __construct()
    {
        $this->obj_model = new UserComment();
    }

    /**
     * @param int $goods_id
     * @param int $limit
     * @return array
     */
    public function getUserCommentsByGoodsId(int $goods_id = 0, int $limit = 10): array
    {
        //根据goods_id获取所有评论
        try {
            $result = $this->obj_model->where('goods_id', $goods_id)
                ->where('parent_id', 0)
                ->paginate($limit)->toArray();
        } catch (\Exception $e) {
            Log::get('userComments-goods', 'error')->error($e->getMessage());
            throw new FooException("查询失败");
        }
        return $result;
    }
}