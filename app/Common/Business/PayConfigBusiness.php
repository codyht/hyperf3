<?php
/**
 * Created by PhpStorm.
 * User: phptao
 * Email: 243194993@qq.com
 * Date: 2024/3/26
 * Time: 21:38
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Common\Business;

use App\Model\GoodsConfig;
use App\Model\PayConfig;

class PayConfigBusiness extends BusBase
{
    protected $obj_model = "";

    public function __construct()
    {
        $this->obj_model = new PayConfig();
    }

    public function add(array $data = []): bool
    {
        foreach ($data as $k => $v) {
            if ($k == 'pay_type') {
                $v = implode(',', $v);
            }
            $obj = new GoodsConfig();
            //查询是否存在
            $res = $obj->where('name', $k)->first();
            if (!$res) {
                $obj->insert([
                    'name' => $k,
                    'value' => $v,
                    'create_time' => time(),
                    'update_time' => time()
                ]);
            } else {
                $obj->where('id', $res['id'])
                    ->update([
                        'value' => $v,
                        'update_time' => time()
                    ]);
            }
        }
        return true;
    }
}