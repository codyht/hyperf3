<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/5/30
 * Time: 9:47
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Common\Lib;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;

class RequestUrl
{
    /**
     * @var RequestInterface
     */
    protected RequestInterface $request;

    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getLocalUrlAndPort(): string
    {
        return $this->request->getUri()->getScheme() . "://" . $this->request->getUri()->getHost() . ":" . $this->request->getUri()->getPort();
    }
}