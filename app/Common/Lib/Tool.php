<?php
/**
 * Created by PhpStorm.
 * User: phptao
 * Email: 243194993@qq.com
 * Date: 2024/3/24
 * Time: 09:40
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Common\Lib;

use Psr\Http\Message\RequestInterface;

class Tool
{
//    use CoroutineSingleTon;

    /**
     * @param $request
     * @return mixed|string
     */
    public static function __ip($request)
    {
        $res = $request->getServerParams();
        if (isset($res['http_client_ip'])) {
            return $res['http_client_ip'];
        }
        if (isset($res['http_x_real_ip'])) {
            return $res['http_x_real_ip'];
        }
        if (isset($res['http_x_forwarded_for'])) {
            // 部分CDN会获取多层代理IP，所以转成数组取第一个值
            $arr = explode(',', $res['http_x_forwarded_for']);
            return $arr[0];
        }
        return $res['remote_addr'];
    }
}