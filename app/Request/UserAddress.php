<?php

declare(strict_types=1);

namespace App\Request;

use Hyperf\Validation\Request\FormRequest;

class UserAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'required:max:20',
            'mobile' => 'required',
            'addressName' => 'required',
            'area' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'is_default' => 'required|boolean'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => '请输入收件人',
            'name.max' => '收件人太长',
            'mobile.required' => '请输入手机号',
            'mobile.regex' => '请输入正确的手机号',
            'addressName.required' => '请选择地址',
            'lat.required' => '请选择地址',
            'lng.required' => '请选择地址',
            'area.required' => '请输入详情地址',
            'is_default.required' => '默认值',
            'is_default.boolean' => '默认值不正确'
        ];
    }
}
