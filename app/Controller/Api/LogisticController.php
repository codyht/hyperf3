<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/5
 * Time: 14:18
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Api;

use App\Common\Business\LogisticBusiness;

class LogisticController extends ApiBaseController
{
    protected LogisticBusiness $obj_bus;

    public function __construct()
    {
        $this->obj_bus = new LogisticBusiness();
    }
}