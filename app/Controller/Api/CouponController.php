<?php
/**
 * Created by PhpStorm.
 * User: phptao
 * Email: 243194993@qq.com
 * Date: 2024/3/30
 * Time: 09:14
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Controller\Api;

use App\Common\Business\CouponBusiness;

class CouponController extends ApiBaseController
{
    protected ?CouponBusiness $obj_bus = null;
    protected array $where = [];

    public function __construct()
    {
        $this->obj_bus = new CouponBusiness();
    }
}