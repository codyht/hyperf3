<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/2/7
 * Time: 15:31
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Controller\Api;

use App\Common\Business\SendSmsBusiness;
use App\Common\Business\UserBusiness;
use App\Common\Lib\Tool;
use \Psr\Http\Message\ResponseInterface;
use App\Common\Lib\Show;
use App\Exception\FooException;

class LoginController extends ApiBaseController
{
    protected ?UserBusiness $obj_bus = null;

    public function __construct()
    {
        $this->obj_bus = new UserBusiness();
    }

    /**
     * 获取请求的ip.
     * @return mixed
     */


    /**
     * 账号和密码登录.
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function doLogin(): ResponseInterface
    {
        $data = [
            'userName' => $this->request->input('userName', ''),
            'password' => $this->request->input('password', ''),
            'code' => $this->request->input('code', ''),
            'ip' => Tool::__ip($this->request),
        ];
        try {
            $res = $this->obj_bus->check($data);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('登录成功', $res);
    }

    /**
     * 发送验证码
     * @return ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function sendCode(): ResponseInterface
    {
        $mobile = $this->request->input('mobile', '');
        try {
            $res = (new SendSmsBusiness())->sendCode($mobile);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('发送成功');
    }

    /**
     * 退出登录.
     * @throws FooException
     */
    public function logout(): ResponseInterface
    {
        try {
            $res = $this->obj_bus->logout();
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('退出成功');
    }

    /**
     * 验证token.
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function verify(): ResponseInterface
    {
        try {
            $res = $this->obj_bus->verify();
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('验证成功', $res);
    }
}
