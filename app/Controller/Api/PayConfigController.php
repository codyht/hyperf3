<?php
/**
 * Created by PhpStorm.
 * User: phptao
 * Email: 243194993@qq.com
 * Date: 2024/3/26
 * Time: 21:36
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);

namespace App\Controller\Api;

use App\Common\Business\PayConfigBusiness;
use App\Common\Lib\Show;

class PayConfigController extends ApiBaseController
{
    protected $obj_bus = null;

    public function __construct()
    {
        $this->obj_bus = new PayConfigBusiness();
    }

    public function list()
    {
        $result = $this->obj_bus->getBaseByListInfo();
        return Show::success('ok', $result);
    }

    public function add()
    {
        $pay_type = $this->request->input('pay_type');
        $data = [
            'pay_type' => $pay_type
        ];
        try {
            $this->obj_bus->add($data);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }
}