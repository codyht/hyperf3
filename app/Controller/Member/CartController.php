<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/1
 * Time: 16:15
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Member;

use App\Common\Business\CartBusiness;
use App\Common\Lib\Show;

class CartController extends MemberBaseController
{
    protected CartBusiness $obj_bus;
    protected array $index_field = ['*'];

    public function __construct()
    {
        $this->obj_bus = new CartBusiness();
    }

    /**
     * @param int $id
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function setUserCart(int $id = 0): \Psr\Http\Message\ResponseInterface
    {
        $type = $this->request->input('type', 'inc');
        $goods_id = $this->request->input('goods_id',0);
        $specs_value = $this->request->input('specs_value','');
        $data = [
            'goods_id'=>$goods_id,
            'specs_value'=>$specs_value
        ];
        try {
            $this->obj_bus->setUserCartByInfo($id, $type,$data);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }

    /**
     * 获取列表
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function list()
    {
        try {
            $result = $this->obj_bus->getUserCartList();
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok', $result);
    }

    /**
     * 删除
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function del()
    {
        $id = $this->request->input('id', 0);
        try {
            $result = $this->obj_bus->delCartInfo($id);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }

    /**
     * 改变数量
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function changeCart()
    {
        $id = $this->request->input('id', 0);
        $number = $this->request->input('number', 0);
        try {
            $result = $this->obj_bus->changeCart($id,$number);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }

    /**
     * 清空购物车
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function clearCart()
    {
        try {
            $result = $this->obj_bus->clearCart();
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }
}