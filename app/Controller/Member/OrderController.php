<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/2
 * Time: 9:56
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Member;

use App\Common\Business\OrderBusiness;
use App\Common\Lib\Show;

class OrderController extends MemberBaseController
{
    protected OrderBusiness $obj_bus;

    public function __construct()
    {
        $this->obj_bus = new OrderBusiness();
    }

    public function confirm()
    {
        $id = $this->request->input('id', '');
        $type = $this->request->input('orderType', 'order');
        try {
            $result = $this->obj_bus->confirm($id, $type);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok', $result);
    }
}