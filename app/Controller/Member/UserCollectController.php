<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/6
 * Time: 11:45
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Member;

use App\Common\Business\UserCollectBusiness;
use App\Common\Lib\Show;

class UserCollectController extends MemberBaseController
{
    protected UserCollectBusiness $obj_bus;

    public function __construct()
    {
        $this->obj_bus = new UserCollectBusiness();
    }

    public function index()
    {
        $limit = $this->request->input('limit', 10);
        $page = $this->request->input('page', 1);
        try {
            $result = $this->obj_bus->getMemberIndex(intval($limit));
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok', $result);
    }
}