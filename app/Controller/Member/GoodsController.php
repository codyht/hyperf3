<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/5/25
 * Time: 16:33
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Member;

use App\Common\Business\GoodsBusiness;
use App\Common\Lib\Show;

class GoodsController extends MemberBaseController
{
    protected GoodsBusiness $obj_bus;
    protected array $index_field = ['*'];

    public function __construct()
    {
        $this->obj_bus = new GoodsBusiness();
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function index()
    {
        $limit = $this->request->input('limit', 10);
        $page = $this->request->input('page', 1);
        $cate_id = $this->request->input('cate_id', '');
        $order_type = $this->request->input('order_type', '');
        $order_value = $this->request->input('order_value', '');
        try {
            $result = $this->obj_bus->getGoodsIndexListInfo(intval($limit), intval($cate_id), $order_type, $order_value);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success("ok", $result);
    }

    /**
     * @param int $id
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function setUserCollect(int $id = 0): \Psr\Http\Message\ResponseInterface
    {
        try {
            $result = $this->obj_bus->setUserCollect($id);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success("收藏成功");
    }

    /**
     * @param int $id
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function info(int $id = 0): \Psr\Http\Message\ResponseInterface
    {
        try {
            $result = $this->obj_bus->getGoodsDetailInfo($id);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success("ok", $result);
    }
}