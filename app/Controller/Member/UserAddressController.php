<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/6/2
 * Time: 10:26
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Member;

use App\Common\Business\UserAddressBusiness;
use App\Common\Lib\Show;
use App\Request\UserAddress;

class UserAddressController extends MemberBaseController
{
    protected UserAddressBusiness $obj_bus;

    public function __construct()
    {
        $this->obj_bus = new UserAddressBusiness();
    }

    public function index()
    {
        $limit = $this->request->input('limit', 10);
        $page = $this->request->input('page', 1);
        try {
            $result = $this->obj_bus->getAddressByListInfo($limit, $page);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok', $result);
    }

    public function add(UserAddress $userAddress)
    {
        $data = $this->request->all();
        try {
            $result = $this->obj_bus->add($data);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }

    public function del(int $id = 0)
    {
        try {
            $result = $this->obj_bus->del($id);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok');
    }
}