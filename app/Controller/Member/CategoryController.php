<?php
/**
 * Created by PhpStorm.
 * User: 小蛮哼哼哼
 * Email: 243194993@qq.com
 * Date: 2023/5/25
 * Time: 15:03
 * motto: 现在的努力是为了小时候吹过的牛逼！
 */

declare(strict_types=1);


namespace App\Controller\Member;

use App\Common\Business\GoodsCategoryBusiness;
use App\Common\Lib\RequestUrl;
use App\Common\Lib\Show;
use Hyperf\Context\ApplicationContext;

class CategoryController extends MemberBaseController
{
    protected GoodsCategoryBusiness $obj_bus;
    protected array $index_field = ['*'];

    public function __construct()
    {
        $this->obj_bus = new GoodsCategoryBusiness();
    }

    /**
     * 获取分类列表
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function index(): \Psr\Http\Message\ResponseInterface
    {
        //":" . $this->request->getUri()->getPort();
        $url = ApplicationContext::getContainer()->get(RequestUrl::class)->getLocalUrlAndPort();
        try {
            $row = $this->obj_bus->getGoodsCategoryIndexByList($url);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success('ok', $row);
    }

    /**
     * @param int $id
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getOneToAll(int $id = 0): \Psr\Http\Message\ResponseInterface
    {
        try {
            $result = $this->obj_bus->getOneToAll($id);
        } catch (\Exception $e) {
            return Show::error($e->getMessage());
        }
        return Show::success("ok", $result);
    }
}