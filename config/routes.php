<?php

declare(strict_types=1);

use App\Controller\Api\GoodsCategoryController;
use App\Controller\Api\GoodsController;
use App\Controller\Api\LoginController;
use App\Controller\Api\LogisticController;
use App\Controller\Api\MenuController;
use App\Controller\Api\RoleController;
use App\Controller\Api\SpecsController;
use App\Controller\Api\SpecsValueController;
use App\Controller\Api\TestController;
use App\Controller\Api\UploadController;
use App\Controller\Api\UploaderCateController;
use App\Controller\Api\UserController;
use App\Controller\CaptchaController;
use App\Controller\Chat\ChatController;
use App\Controller\Member\CartController;
use App\Controller\Member\CategoryController;
use App\Controller\Member\OrderController;
use App\Controller\Member\UserAddressController;
use App\Controller\Member\UserCollectController;
use App\Middleware\HttpAuthMiddleware;
use App\Middleware\MemberAuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');
Router::addServer('http', function () {
    Router::addGroup('/api/', function () {
        // 账号和密码登录
        Router::post('login/doLogin', [LoginController::class, 'doLogin']);
        // 发送验证码
        Router::post('login/sendCode', [LoginController::class, 'sendCode']);
        // 获取验证码
        Router::get('captcha', [CaptchaController::class, 'index']);
    });
    Router::addGroup('/api/', function () {
        // 退出登录
        Router::get('login/logout', [LoginController::class, 'logout']);
        // 验证token
        Router::get('login/verify', [LoginController::class, 'verify']);
        // 添加默认菜单
        Router::post('menu/addMenu', [MenuController::class, 'addMenu']);
        Router::post('menu/getMenuListInfo', [MenuController::class, 'getMenuListInfo']);
        // 获取菜单列表
        Router::post('menu/index', [MenuController::class, 'index']);
        // 获取菜单列表
        Router::post('menu/add', [MenuController::class, 'add']);
        // 获取菜单列表
        Router::get('menu/del', [MenuController::class, 'del']);
        // 获取用户信息 - 根据token获取用户信息
        Router::get('user/getUserInfo', [UserController::class, 'getUserInfo']);
        Router::post('user/index', [UserController::class, 'index']);
        Router::post('user/add', [UserController::class, 'add']);
        Router::post('user/del', [UserController::class, 'del']);
        Router::post('user/getUserListInfo', [UserController::class, 'getUserListInfo']);
        Router::post('user/updateUserInfo', [UserController::class, 'updateUserInfo']);
        Router::post('user/updatePwdInfo', [UserController::class, 'updatePwdInfo']);
        Router::post('user/updateUserAvatar', [UserController::class, 'updateUserAvatar']);
        // 角色相关
        Router::post('role/index', [RoleController::class, 'index']);
        Router::post('role/add', [RoleController::class, 'add']);
        Router::post('role/del', [RoleController::class, 'del']);
        Router::post('role/list', [RoleController::class, 'list']);
        // 上传相关
        Router::post('upload/file', [UploadController::class, 'file']);
        Router::post('upload/index', [UploadController::class, 'index']);
        Router::post('upload/getUploadFile', [UploadController::class, 'getUploadFile']);
        Router::post('upload/mergeData', [UploadController::class, 'mergeData']);
        Router::get('upload/del', [UploadController::class, 'del']);
        Router::post('upload/image', [UploadController::class, 'image']);
        // 上传分类添加
        Router::post('uploadCate/index', [UploaderCateController::class, 'index']);
        Router::post('uploadCate/add', [UploaderCateController::class, 'add']);
        // 商品
        Router::post('goods/index', [GoodsController::class, 'index']);
        Router::post('goods/add', [GoodsController::class, 'add']);
        Router::post('goods/getInfo', [GoodsController::class, 'getInfo']);
        Router::get('goods/del', [GoodsController::class, 'del']);
        // 商品分类
        Router::post('goodsCategory/index', [GoodsCategoryController::class, 'index']);
        Router::post('goodsCategory/add', [GoodsCategoryController::class, 'add']);
        Router::get('goodsCategory/del', [GoodsCategoryController::class, 'del']);
        Router::post('goodsCategory/getGoodsCategoryByList', [GoodsCategoryController::class, 'getGoodsCategoryByList']);
        Router::post('goodsCategory/getGoodsCategoryThreeList', [GoodsCategoryController::class, 'getGoodsCategoryThreeList']);
        // 商品规格
        Router::post('specs/index', [SpecsController::class, 'index']);
        Router::post('specs/add', [SpecsController::class, 'add']);
        Router::get('specs/del', [SpecsController::class, 'del']);
        Router::get('specs/getSpecsByListInfo', [SpecsController::class, 'getSpecsByListInfo']);
        // 规格
        Router::post('specsValue/add', [SpecsValueController::class, 'add']);
        Router::post('specsValue/getSpecsValueByListInfo', [SpecsValueController::class, 'getSpecsValueByListInfo']);
        Router::post('specsValue/getSpecsValueByInfo', [SpecsValueController::class, 'getSpecsValueByInfo']);
        // 运费模板
        Router::post('logistic/index', [LogisticController::class, 'index']);
        //测试
        Router::post('test/index', [TestController::class, 'index']);
        Router::get('test/tt', [TestController::class, 'test']);
        Router::get('test/info', [TestController::class, 'application_info']);
        Router::post('test/ss', [TestController::class, 'ss']);
        //支付配置
        Router::post('payConfig/list', [\App\Controller\Api\PayConfigController::class, 'list']);
        Router::post('payConfig/add', [\App\Controller\Api\PayConfigController::class, 'add']);
        //优惠券相关
        Router::post('coupon/index', [\App\Controller\Api\CouponController::class, 'index']);
    }, ['middleware' => [HttpAuthMiddleware::class]]);
    // 不需要验证权限
    Router::addGroup('/member/', function () {
        Router::post('login/check', [App\Controller\Member\LoginController::class, 'check']);
        Router::post('login/send_msg', [App\Controller\Member\LoginController::class, 'sendMsg']);
        Router::post('category/index', [CategoryController::class, 'index']);
        Router::post('category/getOneToAll/{id}', [CategoryController::class, 'getOneToAll']);
        Router::post('goods/index', [App\Controller\Member\GoodsController::class, 'index']);
    });
    // 需要验证权限
    Router::addGroup('/member/', function () {
        Router::post('user/getUserInfo', [App\Controller\Member\LoginController::class, 'check']);
        Router::post('goods/info/{id}', [App\Controller\Member\GoodsController::class, 'info']);
        // 收藏商品
        Router::post('goods/setUserCollect/{id}', [App\Controller\Member\GoodsController::class, 'setUserCollect']);
        // 购物车相关
        Router::post('cart/setUserCart/{id}', [CartController::class, 'setUserCart']);
        Router::post('cart/list', [CartController::class, 'list']);
        Router::post('cart/del', [CartController::class, 'del']);
        Router::post('cart/changeCart', [CartController::class, 'changeCart']);
        Router::post('cart/clearCart', [CartController::class, 'clearCart']);
        // 订单相关
        Router::post('order/confirm', [OrderController::class, 'confirm']);
        // 地址
        Router::post('address/index', [UserAddressController::class, 'index']);
        Router::post('address/add', [UserAddressController::class, 'add']);
        Router::post('address/del/{id}', [UserAddressController::class, 'del']);
        // 收藏
        Router::post('collect/index', [UserCollectController::class, 'index']);
    }, ['middleware' => [MemberAuthMiddleware::class]]);

    Router::addGroup('/chat/', function () {
        Router::post('chat/index', [ChatController::class, 'chat']);
    });
});
Router::addServer('ws', function () {
    Router::get('/', 'App\Controller\Websocket\WebSocketController');
    Router::get('/login', 'App\Controller\Websocket\WebSocketController');
    Router::get('/message', 'App\Controller\Websocket\WebSocketController');
    Router::get('/list', 'App\Controller\Websocket\WebSocketController');
});
Router::get('/favicon.ico', function () {
    return '';
});
